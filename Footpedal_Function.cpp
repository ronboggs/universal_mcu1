/*
 * CPPFile1.cpp
 * Created: 18-6-2018 15:06:22
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Down_Pedal_Release;
bool Local_Up_Active;

//Function Code.
void Footpedal_Function (void)
{
	if (Robot_Mode_Active_MCU1 == false)	
	{
		//Foot pedal down movement.
		if (Down_Pedal_Release == true && !digitalRead(UP_SOLENOID) == HIGH && digitalRead(FOOT_DOWN1) == HIGH && digitalRead(FOOT_DOWN2) == HIGH && !digitalRead(FOOT_UP) == HIGH && !Non_Conductive_Stop1 == true && Hyd_Cyl_Down_Release_Active == true && Hyd_Cyl_Up_Release_Active == false && Up_Signal_MCU2 == false && Down_Solenoid_Block_MCU2 == false && !Error_Screen_Active_HMI == true && !Error_Set_HMI_MCU2 == true)
		{
			digitalWrite(UP_SOLENOID, LOW);
			digitalWrite(DOWN_SOLENOID, HIGH);
		} 
		//Foot pedal up movement.
		else if (digitalRead(FOOT_UP) == HIGH || (Hyd_Cyl_Up_Release_Active == true) || Up_Signal_MCU2 == true)
		{
			digitalWrite(DOWN_SOLENOID, LOW);
			digitalWrite(UP_SOLENOID, HIGH);
			Local_Up_Active = true;
		}
		//Foot pedal disabled.
		else
		{
			digitalWrite(DOWN_SOLENOID, LOW);
			digitalWrite(UP_SOLENOID, LOW);
		}
		//Ensure Foot pedal needs to be released before Cylinder can move down again after reaching TOS.
		if (Local_Up_Active == true && digitalRead(FOOT_DOWN1) == HIGH)
		{
			Down_Pedal_Release = false;
		}
		else if(digitalRead(FOOT_DOWN1) == LOW)
		{
			Down_Pedal_Release = true;
			Local_Up_Active = false;
		}
	}
	else if (Robot_Mode_Active_MCU1 == true)
	{
		//Cylinder down down movement initiated by Robot. This ensures that it is not possible to have cylinder down movement initiated when machine is not set to Robot Mode.
		if (All_At_Position_MCU2 == true && (Robot_Press_Signal == true || Dwell_Time_Active_MCU2 == true) && !digitalRead(UP_SOLENOID) == HIGH && Hyd_Cyl_Down_Release_Active == true && Hyd_Cyl_Up_Release_Active == false && Up_Signal_MCU2 == false && !Error_Set_HMI_MCU2 == true)
		{
			digitalWrite(UP_SOLENOID, LOW);
			digitalWrite(DOWN_SOLENOID, HIGH);
		}
		//Foot pedal up movement possible in Robot Mode.
		else if (digitalRead(FOOT_UP) == HIGH || (Hyd_Cyl_Up_Release_Active == true) || Up_Signal_MCU2 == true)
		{
			digitalWrite(DOWN_SOLENOID, LOW);
			digitalWrite(UP_SOLENOID, HIGH);
		}
		//Down Output disabled.
		else
		{
			digitalWrite(DOWN_SOLENOID, LOW);
			digitalWrite(UP_SOLENOID, LOW);
		}
	}
}