/*
* Robot_Lower_Tool_Pin_Function.cpp
* Created: 20-9-2018 13:23:13
*  Author: RK
*/

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Robot_Lower_Tool_Pin_Function(void)
{
	if (MCU_Lower_Tool_Pin_Up_Output == true || (MCU_Mode_Key_Switch_Input1 == false && MCU_Mode_Key_Switch_Input2 == false))
	{
		digitalWrite(LTC_SAFE_VALVE, HIGH);
	}
	else
	{
		digitalWrite(LTC_SAFE_VALVE, LOW);
	}
}