/*
 * Conductive_Function.cpp
 * Created: 22-6-2018 10:18:42
 *  Author: RK
 */ 
 
 //Includes
 #include "Main_Siemens_Automation_824MSPe.h"

 //Local Variables.

 //Function Code.
 void Toolingcontact_Function(void)
 {
	 if (digitalRead(CONDUCTIVE_TC) == HIGH)
	 {
		Toolingcontact_Switched_State = true;
	 }
	 else
	 {
		Toolingcontact_Switched_State = false;
	 }
 }