/*
 * Bypass_Function.cpp
 * Created: 19-6-2018 08:20:22
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//CET set value.
int CetValue_Bypass_Function;

//Function Code.
void Bypass_Function(void)
{	
	//Read CET Analog input.
	CetValue_Bypass_Function = Analog_CET;
	
	//Bypass Valve is activated between Bypass Upper Value and Bypass Lower Value.
	if (!Setup_Stroke_HMI == true && (Tooling_Mode_HMI <= 1 || Fastener_Length_HMI == false) && (((CetValue_Bypass_Function < (TOS - Bypass_Upper_Value_HMI)) && digitalRead(UP_SOLENOID) == HIGH) || (CetValue_Bypass_Function > (TPS_Calibrated_Upper_Value_HMI + Bypass_Lower_Value_HMI) && digitalRead(DOWN_SOLENOID) == HIGH)))
	{
		digitalWrite(BYPASS_SOLENOID, HIGH);
	}
	//else if (!Setup_Stroke_HMI == true && (Fastener_Length_HMI == true && Tooling_Mode_HMI == 2) && ((CetValue_Bypass_Function > (Fastener_Length_Calibrated_Value_HMI + Bypass_Upper_Value_HMI))) || digitalRead(UP_SOLENOID) == HIGH)
	else if ((!Setup_Stroke_HMI == true && Machine_Run_State == true && (Fastener_Length_HMI == true && Tooling_Mode_HMI == 2) && ((CetValue_Bypass_Function > (Fastener_Length_Calibrated_Value_HMI + Bypass_Upper_Value_HMI)))) || digitalRead(UP_SOLENOID) == HIGH)
	{
		digitalWrite(BYPASS_SOLENOID, HIGH);
	}
	else
	{
		digitalWrite(BYPASS_SOLENOID, LOW);
	}
}