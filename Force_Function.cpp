/*
 * Force_Function.cpp
 * Created: 19-6-2018 13:55:42
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Force_Function (void)
{
	//Force is based on selection HMI. This means that proportional amplifier is set by HMI software. Coil is only set during down movement (Down_Solenoid active).
	if (DOWN_SOLENOID == true)
	{
		//Analog write value. value 340=1Volt, value 3400=10Volt.
		//900LBS = 800 (int).
		//12000LBS = 2400 (int).
		analogWrite(AO_AMPLIFIER, Force_Set_Value_HMI);
	}
	else
	{
		analogWrite(AO_AMPLIFIER, 0);
	}
}