#include "Main_Siemens_Automation_824MSPe.h"
/*
    Name:       Siemens_Automation_824MSPe.ino
    Created:	18-6-2018 11:55:34
    Author:     RKPC\RK
*/

// Define User Types below here or use a .h file
//
// Define Function Prototypes that use User Types below here or use a .h file
//
// Define Functions below here or use other .ino or cpp files
//
// The setup() function runs once each time the micro-controller starts
//
// This is an C/C++ code to insert repetitive code sections in-line pre-compilation
// Wait for synchronization of registers between the clock domains
// ADC
