/*
 * Robot_Communication_Function.cpp
 * Created: 8-8-2018 14:14:53
 * Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//CET set value.
int CetValue_Robot_Communication_Function;
//Pressure Switch Set value.
int Pressure_Switch_Robot_Communication_Function;

//Function Code.
void Robot_Communication_Function(void)
{
	//*******************Standard IO********************\\
	\\**************************************************//
	
	//Robot Hydraulic Enable.
	//X211.
	if (Robot_Hydraulic_Enabled_Input == true)
	{
		Robot_Hydraulic_Enable = true;
	}
	else
	{
		Robot_Hydraulic_Enable = false;
	}
	//Robot Shuttle Lock.
	//X213
	if (Robot_Shuttle_Lock == true)
	{
		Robot_Shuttle_Lock_MCU2 = true;
	}
	else
	{
		Robot_Shuttle_Lock_MCU2 = false;
	}
	//Robot MAS Feed Fastener.
	//X212
	if (Robot_MAS_Feed_Signal == true)
	{
		Robot_Feed_Fastener_MCU2 = true;
	} 
	else
	{
		Robot_Feed_Fastener_MCU2 = false;
	}
	//Press Signal from Robot. Pulse of 200ms.
	//X225
	if (Robot_Press_Signal == true)
	{
		Robot_Press_Active_MCU1 = true;
		Robot_Press_Active_MCU2 = true;
	} 
	else
	{
		Robot_Press_Active_MCU1 = false;
		Robot_Press_Active_MCU2 = false;
	}
	//Press Complete Signal to Robot.
	//Y224
	if (Press_Complete_MCU2 == true)
	{
		Robot_Press_Complete = true;
	} 
	else
	{
		Robot_Press_Complete = false;
	}
	//All at Position Signal send to Robot. Press signal can be given again after an All at Position Signal.
	//Y220
	if (All_At_Position_MCU2 == true)
	{
		Robot_All_At_Position = true;
	} 
	else
	{
		Robot_All_At_Position = false;
	}
	//EMG Stop Signal Output from Robot to MCU1. Check when Robot is in EMG Stop Mode.
	//Siemens Safety Relay (24V Output) -> ESTOP#1. Pin1.
	//Siemens Safety Relay (24V Output) -> ESTOP#2. Pin3.
	if (E_Stop_EXT_1_Robot == true || E_Stop_EXT_2_Robot == true)
	{
		Robot_EMG_Error_Set = true;
	} 
	else
	{
		Robot_EMG_Error_Set = false;
	}
	//EMG Stop signal Input to Robot. EMG Stop Output Signal.
	//EMG Output#1 -> Safety Relay Siemens.
	//EMG Output#2 -> Safety Relay Siemens.
	//Signal from MCU2.
	if (EMG_System_Active_HMI == true)
	{
		EMG_Insertion_machine_Active = true;
	} 
	else
	{
		EMG_Insertion_machine_Active = false;
	}
	//EMG Stop Reset Signal from Robot.
	//X226.
	if (Robot_EMG_Error_Reset)
	{
		EMG_Error_Reset_MCU1 = true;
		EMG_Error_Reset_MCU2 = true;
	} 
	else
	{
		EMG_Error_Reset_MCU1 = false;
		EMG_Error_Reset_MCU2 = false;
	}
	//Haeger Error Output signal to Robot. Set Error Bit. Error displayed at HMI.
	//Y210-Y215.
	if (Error_Set_HMI_MCU2 == true)
	{
		Robot_Error_Set = true;
	} 
	else
	{
		Robot_Error_Set = false;
	}
	//Error Reset Input from Robot. Will be send to MCU2.
	//X210.
	if (Robot_Error_Reset_Input == true)
	{
		Robot_Error_Reset_MCU2 = true;
		Robot_Error_Reset_HMI = true;
	} 
	else
	{
		Robot_Error_Reset_MCU2 = false;
		Robot_Error_Reset_HMI = false;
	}
	//Key Switch Input at MCU2 (2x).
	//X226.
	//X227.
	if (MCU_Mode_Key_Switch_Input1 == true && MCU_Mode_Key_Switch_Input2 == true)
	{
		Robot_Mode_Active_MCU1 = true;
		//Internal Robot Mode Variable.
		Robot_Mode_Active_MCU2 = true;
		//Robot Mode output to Robot #1.
		Robot_Mode_Active_Output1 = true;
		//Robot Mode output to Robot #2.
		Robot_Mode_Active_Output2 = true;
	}
	//Key Switch Output (2x) to Robot.
	//Y226.
	//Y227.
	else
	{
		Robot_Mode_Active_MCU1 = false;
		//Internal Robot Mode Variable.
		Robot_Mode_Active_MCU2 = false;
		//Robot Mode output to Robot #1.
		Robot_Mode_Active_Output1 = false;
		//Robot Mode output to Robot #2.
		Robot_Mode_Active_Output2 = false;
	}
	//Lower Tool Pin Up. Sensor Output Signal to Robot.
	//Y221.
	//Also possible to have direct output by Pin:66 Harting Connector.
	if (MCU_Lower_Tool_Pin_Sensor_Up_Input == true)
	{
		Robot_Lower_Tool_Pin_Sensor_Up_Output = true;
		//Lower Tool Pin Position send to MCU2.
		Lower_Tool_Pin_Up_Sensor_MCU2 = true;
	} 
	else
	{
		Robot_Lower_Tool_Pin_Sensor_Up_Output = false;
		//Lower Tool Pin Position send to MCU2.
		Lower_Tool_Pin_Up_Sensor_MCU2 = false;
	}
	//Lower Tool Pin Down. Sensor Output Signal to Robot.
	//Y222.
	//Not possible to have direct output by Harting Connector.
	if (MCU_Lower_Tool_Pin_Sensor_Down_Input == true)
	{
		Robot_Lower_Tool_Pin_Sensor_Down_Output = true;
		//Lower Tool Pin Position send to MCU2.
		Lower_Tool_Pin_Down_Sensor_MCU2 = true;
	} 
	else
	{
		Robot_Lower_Tool_Pin_Sensor_Down_Output = false;
		//Lower Tool Pin Position send to MCU2.
		Lower_Tool_Pin_Down_Sensor_MCU2 = false;
	}
	//Robot Input Signal to move Lower Tool Pin Up. Lower Tool Pin is controlled by Robot.
	//X221.
	if (Robot_Lower_Tool_Pin_Up == true)
	{
		MCU_Lower_Tool_Pin_Up_Output = true;
	}
	else
	{
		MCU_Lower_Tool_Pin_Up_Output = false;
	}
	//Robot Input Signal to move Lower Tool Pin Down. Lower Tool Pin is controlled by Robot. Only needed with 5/2 valve. Not integrated.
	//X222.
	if (Robot_Lower_Tool_Pin_Down == true)
	{
		MCU_Lower_Tool_Pin_Down_Output = true;
	}
	else
	{
		MCU_Lower_Tool_Pin_Down_Output = false;
	}
	//Receive Shuttle Sensor Retract Position from MCU2. Then send with Outputs to Robot.
	if (Shuttle_Retract_MCU2 == true)
	{
		Shuttle_Retract_MCU1 = true;
	} 
	else
	{
		Shuttle_Retract_MCU1 = false;
	}
	//Receive Shuttle Sensor Extend Position from MCU2. Then send with Outputs to Robot.
	if (Shuttle_Extend_MCU2 == true)
	{
		Shuttle_Extend_MCU1 = true;
	}
	else
	{
		Shuttle_Extend_MCU1 = false;
	}
	// Send TOS as digital output to give ACK to Robot that Cylinder achieved TOS.
	if (CetValue_Robot_Communication_Function > (TOS + 50) && digitalRead(DOWN_SOLENOID) == LOW)
	{
		Hydraulic_Cylinder_TOS_Robot = true;
	} 
	else
	{
		Hydraulic_Cylinder_TOS_Robot = false;
	}
	//*******************Additional Siemens requested IO********************\\
	\\**********************************************************************//

	//Analog CET Output to Robot. Signal is continuesly On.
	//AO1.Pin 11
	
	//Read analog CET value.
	CetValue_Robot_Communication_Function = analogRead(AI_CET);

	if (Machine_Init_State == true)
	{
		Robot_CetValue_Output = CetValue_Robot_Communication_Function;
	} 
	else
	{
		Robot_CetValue_Output = 0;
	}
	//Analog Pressure switch Signal. Signal is continuesly On. Siemens can decide when to use this value. For example to detect applied force at Press Complete.
	//AO0.Pin 10.
	
	//Read analog Pressure Sensor value.
	Pressure_Switch_Robot_Communication_Function = analogRead(AI_PRESSURE_SWITCH);

	if (Machine_Init_State == true)
	{
		Robot_Pressure_Switch_Output = Pressure_Switch_Robot_Communication_Function;
	} 
	else
	{
		Robot_Pressure_Switch_Output = 0;	
	}
}