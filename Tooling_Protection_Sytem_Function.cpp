/*
 * TPS_Function.cpp
 * Created: 3-7-2018 13:33:53
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool TPS_Lock;

//CET set value.
int CetValue_TPS_Function;

//Function Code.
void Tooling_Protection_System_Function(void)
{
	//Read CET Analog input.
	CetValue_TPS_Function = Analog_CET;
	
	//TPS Measurement Active to ensure no error message will displayed during setup.
	//if (!Setup_Stroke_HMI == true && !digitalRead(UP_SOLENOID == true))
	if (!TPS_Lock == true)
	
		//TPS function is always active. Not possible to deactivate.
		//if (TPS_Error == true || ((Safety_Sensor_Switched_State == true && (CetValue_TPS_Function < (TPS_Calibrated_Lower_Value_HMI - TPS_Offset_Value_HMI))) || (Safety_Sensor_Switched_State == true && (CetValue_TPS_Function > (TPS_Calibrated_Upper_Value_HMI + TPS_Offset_Value_HMI)))))
		if (TPS_Error == true || (Safety_Sensor_Switched_State == true && (CetValue_TPS_Function > (TPS_Calibrated_Upper_Value_HMI + TPS_Offset_Value_HMI))))
		{
			TPS_Error = true;
		}
		else if (TPS_Error == true || (Safety_Sensor_Switched_State == false && (CetValue_TPS_Function < (TPS_Calibrated_Upper_Value_HMI - TPS_Offset_Value_HMI))))
		{
			TPS_Error = true;
		}
	//Ensure no TPS error is generated during Setup Stroke (While using Dwell Time).	
	if (Setup_Stroke_HMI == true)
	{		
		if (Up_Signal_MCU2 == false)
		{
			TPS_Lock = true;
		}
		if (Up_Signal_MCU2 == true)
		{
			TPS_Lock = false;
		}
	}
	else if (Up_Signal_MCU2 == true || Machine_Idle_State == true)
	{
		TPS_Lock = false;
	}
}