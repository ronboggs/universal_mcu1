/*
 * Hydraulic_Cylinder_Function.cpp
 * Created: 17-7-2018 17:16:21
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool TPS_Cylinder_Lock;

//Timer Variables.
unsigned long Timer_1;
//Set timer to true when timer is finished.
bool TimedOut_1 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_1;
//Reset Time.
int TPS_Lock_Reset_Time = 1000;
//TPS Lock Reset
bool TPS_Lock_Reset;

//CET set value.
int CetValue_Hydraulic_Cylinder_Function;

//Setup Code.
void Setup_Hydraulic_Cylinder_Function(void)
{
	//Timer initialization.
	TimedOut_1 = false;
	//Start Timer.
	Timer_1 = millis();
}

//Function Code.
void Hydraulic_Cylinder_Function(void)
{
	//Read CET Analog input.
	CetValue_Hydraulic_Cylinder_Function = Analog_CET;
	
	// Stop Hydraulic Cylinder Down Movement after Error. Not possible to move cylinder down when TPS is not Set.
	if (Error_Screen_Active_HMI == true || TPS_Cylinder_Lock == true || Safety_Sensor_Error == true || SSSF_Safety_Sensor_Error == true || TPS_Error == true || !EMG_System_Active_HMI == true)
	{
		Hyd_Cyl_Down_Release_Active = false;
	}
	else
	{
		Hyd_Cyl_Down_Release_Active = true;
	}
	//Hydraulic Cylinder Up Movement until TOS (Top of Stroke) after Error.
	if (Hyd_Cyl_Down_Release_Active == false && (TOS >= CetValue_Hydraulic_Cylinder_Function))
	{
		Hyd_Cyl_Up_Release_Active = true;
	}
	else
	{
		Hyd_Cyl_Up_Release_Active = false;
	}
	//Reset Errors above TOS.
	if (CetValue_Hydraulic_Cylinder_Function >= TOS)
	{
		//TPS error will be reset at TOS.
		TPS_Error = false;
		//SS error will be reset at TOS.
		Safety_Sensor_Error = false;
	}
	//TPS Setup Cylinder Lock Timer.
	if ((TimedOut_1 == true) && ((millis() - Timer_1) > INTERVAL_1))
	{
		//Timed out to deactivate timer.
		TimedOut_1 = true;
		
		//Check if Swap Bit is activated.
		if (TPS_Lock_Reset == true)
		{
			//De-active Timer.
			TPS_Lock_Reset = false;
			TimedOut_1 = false;
		}
	}
	//Set Swap Bit Time.
	INTERVAL_1 = TPS_Lock_Reset_Time;
	
	//Timer Start.
	if (Setup_Stroke_HMI == true && TimedOut_1 == false)
	{
		TimedOut_1 = true;
		Timer_1 = millis();
		TPS_Lock_Reset = true;
	}
	//TPS Setup Cylinder Lock. TPS lock will ensure that it is not possible to move the Ram down when TPS is not set.
	if (Setup_Stroke_HMI == false && !TPS_Lock_Reset == true && (TPS_Calibrated_Upper_Value_HMI == 0 || TPS_Calibrated_Lower_Value_HMI == 0))
	{
		if (TPS_Calibrated_Upper_Value_HMI == 0 || TPS_Calibrated_Lower_Value_HMI == 0)
		{
			TPS_Cylinder_Lock = true;
		}
	}
	else if (Setup_Stroke_HMI == true)
	{
		TPS_Cylinder_Lock = false;
	}
	else if (Setup_Stroke_HMI == false && (TPS_Calibrated_Upper_Value_HMI > 0 || TPS_Calibrated_Lower_Value_HMI > 0))
	{
		TPS_Cylinder_Lock = false;
	}
}