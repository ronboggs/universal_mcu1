/*
 * Conductive_Mode_Function.cpp
 * Created: 22-6-2018 09:51:28
 *  Author: RK
 */
 
//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Conductive_Mode_Function(void)
{
	if (ConductiveM_HMI == false)
	{
		if (Safety_Sensor_Error == true || Safety_Sensor_Switched_State == true && Toolingcontact_Switched_State == false)
		{
			Safety_Sensor_Error = true;
		}
	}
}