/*
 * Buzzer_Function.cpp
 * Created: 28-6-2018 08:11:39
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Swap_Buzzer_Bit;
bool Buzzer_Active;

//Timer Variables.
unsigned long Timer_0;			
//Set timer to true when timer is finished.
bool TimedOut_0 = false;
//Amount of time the Timer is active.			
unsigned long INTERVAL_0;


//Setup Code.
void Setup_Buzzer_Function(void)
{
	//Timer initialization.
	TimedOut_0 = false;
	//Start Timer.
	Timer_0 = millis();
}

//Function Code.
void Buzzer_Function(void)
{
	//Activate Buzzer for 2000ms.
	if ((TimedOut_0 == true) && ((millis() - Timer_0) > INTERVAL_0)) 
	{
		//Timed out to deactivate timer.
		TimedOut_0 = true;

		//Check if Buzzer is activated.
		if (Swap_Buzzer_Bit == true)
		{
			//De active Buzzer.
			TimedOut_0 = false;
			Buzzer_Active = false;
		} 
	}
	//Set Swap Bit Time.
	INTERVAL_0 = Buzzer_Beep_Time_HMI;
	
	//Timer Start. Activate Buzzer.
	if (Buzzer_HMI == true && TimedOut_0 == false && !Swap_Buzzer_Bit == true)
	{		
		TimedOut_0 = true;
		Timer_0 = millis();
		Buzzer_Active = true;
		Swap_Buzzer_Bit = true;
	}
	
	//Swap Buzzer_HMI bit.
	if (Buzzer_HMI == true && !Buzzer_Active == true)
	{
		Buzzer_HMI = 0;
		Read_Coil_4 = false;
		Write_Coil_4 = true;
		Swap_Buzzer_Bit = false;
	}
	else
	{
		Write_Coil_4 = false;
		Read_Coil_4 = true;
	}
	//Active / De active Buzzer.
	if (Buzzer_Active == true)
	{
		digitalWrite(BUZZER, HIGH);
	}
	else
	{
		digitalWrite(BUZZER, LOW);
	}
}