/*
 * Safety_Block_Function.cpp
 * Created: 28-6-2018 08:23:30
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Safety_Block_Function(void)
{
	if (digitalRead(ESTOP2) == HIGH && Robot_EMG_Error_Set == true && EMG_System_Active_MCU2 == true)
	{
		EMG_System_Active_HMI = true;
	} 
	else
	{
		EMG_System_Active_HMI = false;
	}
}